--liquibase formatted sql

--changeset rife:insert_users

INSERT INTO USERS(LOGIN, EMAIL, PASSWORD, PREV_PASSWORD, CREATED_BY_SYSTEM, CREATED_AT, FIRST_AUTH)
VALUES ('katya', 'katya@fake.com', '{bcrypt}$2a$12$Uk3LJPeh2IVaMYV4Zwq0zuv00NxaIbhYNqLKQFn3/0Quan2lsbsKe', null, 'SELF_REGISTERED', current_timestamp, false);