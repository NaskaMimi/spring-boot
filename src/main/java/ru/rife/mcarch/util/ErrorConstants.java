package ru.rife.mcarch.util;

public final class ErrorConstants {
    private ErrorConstants() {}

    public static final String ERROR_USER_INPUT = "Ошибка ввода";
}