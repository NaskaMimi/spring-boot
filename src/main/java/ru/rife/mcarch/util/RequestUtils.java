package ru.rife.mcarch.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.rife.mcarch.param.MyCountryArchitectParams;

import java.net.URI;
import java.util.UUID;

@Component
public class RequestUtils {
    private final MyCountryArchitectParams myCountryArchitectParams;

    @Autowired
    public RequestUtils(MyCountryArchitectParams myCountryArchitectParams) {
        this.myCountryArchitectParams = myCountryArchitectParams;
    }

    public String buildRequestConfirmRegistration(UUID id) {
        return ServletUriComponentsBuilder.fromCurrentRequest().replacePath("/api/v1/user/confirm-register-user")
                .queryParam("id", String.valueOf(id))
                .build().toUriString();
    }

    public URI buildRequestAfterAcceptRegistration() {
        return myCountryArchitectParams.getRedirectAfterAcceptRegistration();
    }

    public URI buildRequestAfterChangePassword() {
        return myCountryArchitectParams.getRedirectAfterChangePassword();
    }

    public URI buildRequestAfterChangeEmail() {
        return myCountryArchitectParams.getRedirectAfterChangeEmail();
    }

    public String buildRequestConfirmChangePassword(UUID changeEmailRequestId) {
        return ServletUriComponentsBuilder.fromCurrentRequest().replacePath("/api/v1/user/confirm-change-password")
                .queryParam("id", String.valueOf(changeEmailRequestId))
                .build().toUriString();
    }

    public String buildRequestConfirmChangeEmail(UUID changeEmailRequestId) {
        return ServletUriComponentsBuilder.fromCurrentRequest().replacePath("/api/v1/user/confirm-change-email")
                .queryParam("id", String.valueOf(changeEmailRequestId))
                .build().toUriString();
    }
}