package ru.rife.mcarch.util.validation;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Objects;

public class CombinedNotNullValidator
        implements ConstraintValidator<CombinedNotNull, Object> {
    private String[] fields;

    @Override
    public void initialize(final CombinedNotNull combinedNotNull) {
        fields = combinedNotNull.fields();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext ctx) {
        final BeanWrapperImpl beanWrapper = new BeanWrapperImpl(value);
        return Arrays.stream(fields)
                .map(beanWrapper::getPropertyValue)
                .anyMatch(Objects::nonNull);
    }
}