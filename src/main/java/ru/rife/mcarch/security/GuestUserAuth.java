package ru.rife.mcarch.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import ru.rife.mcarch.param.MyCountryArchitectParams;

@Component
public class GuestUserAuth {
    private final MyCountryArchitectParams myCountryArchitectParams;

    @Autowired
    public GuestUserAuth(MyCountryArchitectParams myCountryArchitectParams) {
        this.myCountryArchitectParams = myCountryArchitectParams;
    }

    public UserDetails generateTokenForGuest(String login) {
        if (!(myCountryArchitectParams.getGuestLogin().equals(login))) {
            return null;
        }
        return org.springframework.security.core.userdetails.User.builder()
                .username(login)
                .password(myCountryArchitectParams.getGuestPassword())
                .roles("GUEST")
                .build();
    }
}
