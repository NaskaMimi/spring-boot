package ru.rife.mcarch.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.rife.mcarch.dto.ChangeEmailRequest;
import ru.rife.mcarch.dto.ChangePasswordRequest;
import ru.rife.mcarch.dto.CreateUserRequest;
import ru.rife.mcarch.service.UserService;

import javax.validation.Valid;
import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/registration")
    public ResponseEntity<Void> registeredUser(@RequestBody @Valid CreateUserRequest createUserRequest) {
        userService.registeredUser(createUserRequest);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/change-password")
    public ResponseEntity<Void> changePassword(@RequestBody @Valid ChangePasswordRequest changePasswordRequest) {
        userService.createChangePasswordRequest(changePasswordRequest);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/change-email")
    public ResponseEntity<Void> changeEmail(@RequestBody @Valid ChangeEmailRequest changeEmailRequest) {
        userService.createChangeEmailRequest(changeEmailRequest);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/confirm-register-user")
    public ResponseEntity<Void> confirmRegisterUser(@RequestParam UUID id) {
        URI redirectUri = userService.confirmRegisterUserByRequest(id);
        return ResponseEntity
                .status(HttpStatus.SEE_OTHER)
                .location(redirectUri)
                .build();
    }

    @GetMapping("/confirm-change-password")
    public ResponseEntity<Void> confirmChangePassword(@RequestParam UUID id) {
        URI redirectUri = userService.confirmChangePassword(id);
        return ResponseEntity.
                status(HttpStatus.SEE_OTHER)
                .location(redirectUri)
                .build();
    }

    @GetMapping("/confirm-change-email")
    public ResponseEntity<Void> confirmChangeEmail(@RequestParam UUID id) {
        URI redirectUri = userService.confirmChangeEmail(id);
        return ResponseEntity
                .status(HttpStatus.SEE_OTHER)
                .location(redirectUri)
                .build();
    }
}