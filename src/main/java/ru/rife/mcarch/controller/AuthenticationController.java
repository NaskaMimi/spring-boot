package ru.rife.mcarch.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.rife.mcarch.dto.AuthRequest;
import ru.rife.mcarch.dto.AuthDataResponse;
import ru.rife.mcarch.service.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthService authService;

    @PostMapping(value = "/login")
    public ResponseEntity<AuthDataResponse> createAuthenticationToken(@RequestBody @Valid AuthRequest authenticationRequest) {
        AuthDataResponse authDataResponse = authService.authByLoginAndPassword(authenticationRequest.getLogin(), authenticationRequest.getPassword());
        return ResponseEntity.ok(authDataResponse);
    }
}