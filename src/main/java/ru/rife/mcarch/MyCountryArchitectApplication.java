package ru.rife.mcarch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import ru.rife.mcarch.param.MyCountryArchitectParams;

@SpringBootApplication
@EnableConfigurationProperties({MyCountryArchitectParams.class})
public class MyCountryArchitectApplication {
    public static void main(String[] args) {
        SpringApplication.run(MyCountryArchitectApplication.class, args);
    }
}