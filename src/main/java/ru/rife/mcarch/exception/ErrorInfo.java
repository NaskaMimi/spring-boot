package ru.rife.mcarch.exception;

import lombok.*;

@Value
public class ErrorInfo {
    String title;
    String detail;
}
