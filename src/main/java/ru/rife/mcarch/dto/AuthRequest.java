package ru.rife.mcarch.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Value
public class AuthRequest {
    @NotNull
    @Size(min = 5)
    String login;
    @NotNull
    String password;
}