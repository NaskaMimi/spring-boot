package ru.rife.mcarch.dto;

import lombok.Value;
import ru.rife.mcarch.util.validation.CombinedNotNull;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Value
@CombinedNotNull(fields = {
        "login",
        "email"
}, message = "Неуказан login или email")
public class ChangePasswordRequest {
    @Size(min = 5)
    String login;
    @Email
    String email;
    @NotNull
    @Size(min = 5)
    String newPassword;
}
