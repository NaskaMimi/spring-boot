package ru.rife.mcarch.dto;

import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Value
public class ChangeEmailRequest {
    @NotNull
    @Email
    String oldEmail;
    @NotNull
    @Email
    String newEmail;
}
