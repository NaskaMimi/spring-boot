package ru.rife.mcarch.dto;

import lombok.NonNull;
import lombok.Value;

import java.util.List;

@Value
public class AuthDataResponse {
    @NonNull
    String jwtToken;
    long expiredAt;
    @NonNull
    List<String> roles;
}