package ru.rife.mcarch.configuration;

import lombok.RequiredArgsConstructor;
import org.passay.PasswordGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.rife.mcarch.security.JwtUtil;
import ru.rife.mcarch.service.DbUserDetailService;
import ru.rife.mcarch.param.MyCountryArchitectParams;
import ru.rife.mcarch.security.JwtRequestFilter;
import ru.rife.mcarch.security.oath2.JwtTokenProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final MyCountryArchitectParams myCountryArchitectParams;
    private final DbUserDetailService dbUserDetailService;
    private final JwtRequestFilter jwtRequestFilter;
    private final List<JwtTokenProvider> jwtTokenProviders;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(dbUserDetailService);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        String idForEncode = "bcrypt";
        Map<String, PasswordEncoder> encoders = new HashMap<>();
        encoders.put(idForEncode, new BCryptPasswordEncoder());

        return new DelegatingPasswordEncoder(idForEncode, encoders);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(dbUserDetailService);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/api/v1/auth/**", "/api/v1/user/**")
                .permitAll()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                .anyRequest().authenticated().and()
                .oauth2Login()
                .successHandler((request, response, authentication) -> {
                    if (authentication instanceof OAuth2AuthenticationToken oAuth2AuthenticationToken) {
                        JwtUtil.JwtRich jwtRich = jwtTokenProviders.stream()
                                .filter(e -> e.equalsSystemId(oAuth2AuthenticationToken))
                                .findAny()
                                .map(e -> e.jwtTokenGenerate(oAuth2AuthenticationToken))
                                .orElseThrow(() -> new IllegalStateException("Not support oath system"));

                        response.sendRedirect(myCountryArchitectParams.getRedirectSuccessOAuthUrl() +
                                "?jwtToken=" + jwtRich.getToken() + "&role=USER" + "&expiredAt=" + jwtRich.getExpiredAt());
                    }
                })
                .and()
                .exceptionHandling().and().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public PasswordGenerator passwordGenerator() {
        return new PasswordGenerator();
    }
}