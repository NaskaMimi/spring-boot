package ru.rife.mcarch.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.user.OAuth2User;
import ru.rife.mcarch.service.UserService;

@Configuration
@RequiredArgsConstructor
public class ExternalAuthSystemConfig {
    private final UserService userService;

    @Bean
    public OAuth2UserService<OAuth2UserRequest, OAuth2User> oauth2UserService() {
        DefaultOAuth2UserService delegate = new DefaultOAuth2UserService();
        return request -> {
            OAuth2User user = delegate.loadUser(request);
            if ("google".equals(request.getClientRegistration().getRegistrationId())) {
                userService.saveFromGoogleResponse(user);
                return user;
            }

            throw new OAuth2AuthenticationException(new OAuth2Error("invalid_token", "Forbidden", ""));
        };
    }
}