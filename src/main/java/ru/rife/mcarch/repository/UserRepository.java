package ru.rife.mcarch.repository;

import org.springframework.data.jpa.repository.Query;
import ru.rife.mcarch.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByLogin(String login);

    User findByEmail(String email);

    @Query(value = "from User where email = :email or googleEmail = :email")
    User findByEmailOrGoogleEmail(String email);
}
