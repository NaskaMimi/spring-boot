package ru.rife.mcarch.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rife.mcarch.entity.ChangePasswordRequestEntity;

import java.util.UUID;

public interface ChangePasswordRequestRepository extends JpaRepository<ChangePasswordRequestEntity, UUID> { }