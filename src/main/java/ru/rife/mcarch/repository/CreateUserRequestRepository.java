package ru.rife.mcarch.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rife.mcarch.entity.CreateUserRequestEntity;

import java.util.UUID;

public interface CreateUserRequestRepository extends JpaRepository<CreateUserRequestEntity, UUID> {
}
