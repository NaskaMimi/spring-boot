package ru.rife.mcarch.param;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.net.URI;

@Data
@ConfigurationProperties(prefix = "my-country-architect")
public class MyCountryArchitectParams {
    private URI redirectSuccessOAuthUrl;
    private URI redirectAfterAcceptRegistration;
    private URI redirectAfterChangePassword;
    private URI redirectAfterChangeEmail;
    private String mailerUsername;
    private String mailerPassword;
    private String guestLogin;
    private String guestPassword;
}