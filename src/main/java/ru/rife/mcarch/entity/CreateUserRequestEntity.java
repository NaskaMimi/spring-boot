package ru.rife.mcarch.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "CREATE_USER_REQUESTS")
@Getter
@NoArgsConstructor
public class CreateUserRequestEntity {
    @Id
    @Column(name = "ID")
    private UUID id;
    @Column(name = "LOGIN", nullable = false, length = 100)
    private String login;
    @Email
    @Column(name = "EMAIL", nullable = false, length = 150)
    private String email;
    @Column(name = "CREATED_AT", nullable = false)
    private LocalDateTime createdAt;

    public CreateUserRequestEntity(String login, String email) {
        this.id = UUID.randomUUID();
        this.login = login;
        this.email = email;
        this.createdAt = LocalDateTime.now();
    }

    public boolean checkValidDate() {
        return createdAt.isAfter(LocalDateTime.now());
    }
}