package ru.rife.mcarch.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@NoArgsConstructor
@Entity
@Table(name = "CHANGE_EMAIL_REQUESTS")
public class ChangeEmailRequestEntity {
    @Id
    @Column(name = "ID", nullable = false)
    private UUID id;
    @ManyToOne(optional = false)
    @JoinColumn(name = "USER_ID", nullable = false)
    private User user;
    @Email
    @Column(name = "OLD_EMAIL", nullable = false, length = 150)
    private String oldEmail;
    @Email
    @Column(name = "NEW_EMAIL", nullable = false, length = 150)
    private String newEmail;
    @Column(name = "CREATED_AT", nullable = false)
    private LocalDateTime createdAt;

    public ChangeEmailRequestEntity(User user,
                                    String oldEmail,
                                    String newEmail) {
        this.id = UUID.randomUUID();
        this.user = user;
        this.oldEmail = oldEmail;
        this.newEmail = newEmail;
        this.createdAt = LocalDateTime.now();
    }

    public boolean isValidDate() {
        return createdAt.isAfter(LocalDateTime.now());
    }
}
