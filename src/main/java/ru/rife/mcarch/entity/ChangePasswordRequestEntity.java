package ru.rife.mcarch.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@NoArgsConstructor
@Entity
@Table(name = "CHANGE_PASSWORD_REQUESTS")
public class ChangePasswordRequestEntity {
    @Id
    @Column(name = "ID", nullable = false)
    private UUID id;
    @ManyToOne(optional = false)
    @JoinColumn(name = "USER_ID", nullable = false)
    private User user;
    @Column(name = "PASSWORD", nullable = false)
    private String password;
    @Column(name = "CREATED_AT", nullable = false)
    private LocalDateTime createdAt;

    public ChangePasswordRequestEntity(User user,
                                       String password) {
        this.id = UUID.randomUUID();
        this.user = user;
        this.password = password;
        this.createdAt = LocalDateTime.now();
    }

    public boolean checkValidDate() {
        return createdAt.isAfter(LocalDateTime.now());
    }
}