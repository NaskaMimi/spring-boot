package ru.rife.mcarch.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import ru.rife.mcarch.dto.ChangeEmailRequest;
import ru.rife.mcarch.dto.ChangePasswordRequest;
import ru.rife.mcarch.dto.CreateUserRequest;
import ru.rife.mcarch.entity.*;
import ru.rife.mcarch.exception.ControlFailedException;
import ru.rife.mcarch.exception.ErrorInfo;
import ru.rife.mcarch.exception.NotFoundException;
import ru.rife.mcarch.exception.RedirectException;
import ru.rife.mcarch.param.MyCountryArchitectParams;
import ru.rife.mcarch.repository.ChangeEmailRequestRepository;
import ru.rife.mcarch.repository.ChangePasswordRequestRepository;
import ru.rife.mcarch.repository.CreateUserRequestRepository;
import ru.rife.mcarch.repository.UserRepository;
import ru.rife.mcarch.util.ErrorConstants;
import ru.rife.mcarch.util.RequestUtils;

import java.net.URI;
import java.util.UUID;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class UserService {
    private static final String USER_EXISTS_BY_EMAIL = "Пользователь с указанным email уже существует";
    private static final String USER_EXISTS_BY_LOGIN = "Пользователь с указанным login уже существует";
    private static final String NOT_FOUND_USER_FOR_SEND_EMAIL = "Не удалось найти пользователя для отправки emial";

    private final UserRepository userRepository;
    private final PasswordGeneratorService passwordGeneratorService;
    private final CreateUserRequestRepository createUserRequestRepository;
    private final ChangePasswordRequestRepository changePasswordRequestRepository;
    private final ChangeEmailRequestRepository changeEmailRequestRepository;
    private final PasswordEncoder passwordEncoder;
    private final EmailService emailService;
    private final RequestUtils requestUtils;
    private final MyCountryArchitectParams myCountryArchitectParams;

    public User findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void updateUserByEmail(String email,
                                  User newUser) {
        User user = findByLogin(email);
        if (user == null) {
            return;
        }
        userRepository.save(newUser);
    }

    public void createChangePasswordRequest(ChangePasswordRequest changePasswordRequest) {
        User user = findUserByLoginOrEmail(changePasswordRequest.getEmail(),
                changePasswordRequest.getLogin());
        String encodePassword = passwordEncoder.encode(changePasswordRequest.getNewPassword());
        ChangePasswordRequestEntity changePasswordRequestEntity = new ChangePasswordRequestEntity(user,
                encodePassword
        );
        changePasswordRequestRepository.save(changePasswordRequestEntity);
        emailService.sendChangePassword(changePasswordRequest.getEmail(),
                changePasswordRequestEntity.getId());
    }

    public void registeredUser(CreateUserRequest user) {
        User checkUser = userRepository.findByEmail(user.getEmail());
        if (checkUser != null) {
            throw new ControlFailedException("user exists by email = " + user.getEmail(),
                    new ErrorInfo(ErrorConstants.ERROR_USER_INPUT, USER_EXISTS_BY_EMAIL));
        }
        checkUser = userRepository.findByLogin(user.getLogin());
        if (checkUser != null) {
            throw new ControlFailedException("user exists by login = " + user.getLogin(),
                    new ErrorInfo(ErrorConstants.ERROR_USER_INPUT, USER_EXISTS_BY_LOGIN));
        }
        CreateUserRequestEntity savedRequest = createUserRequestRepository.save(new CreateUserRequestEntity(
                user.getLogin(), user.getEmail()
        ));
        emailService.sendApproveRegistration(savedRequest.getEmail(), savedRequest.getId());
    }

    public void saveFromGoogleResponse(OAuth2User user) {
        String email = user.getAttribute("email");
        User foundUser = userRepository.findByEmailOrGoogleEmail(email);
        if (foundUser != null) {
            return;
        }
        User newUser = new User(email,
                null, email,
                CreatedBySystem.GOOGLE_OATH2);
        newUser.setGoogleEmail(email);
        userRepository.save(newUser);
    }

    public URI confirmRegisterUserByRequest(UUID id) {
        CreateUserRequestEntity createUserRequestEntity = createUserRequestRepository.findById(id)
                .orElseThrow(NotFoundException::new);

        String originalPass = passwordGeneratorService.generatePassword();
        String encodePass = passwordEncoder.encode(originalPass);
        User newUser = new User(createUserRequestEntity.getLogin(),
                encodePass, createUserRequestEntity.getEmail(), CreatedBySystem.SELF_REGISTERED);
        userRepository.save(newUser);
        emailService.sendNewPasswordToEmail(newUser.getEmail(), originalPass);
        return requestUtils.buildRequestAfterAcceptRegistration();
    }

    public URI confirmChangePassword(UUID id) {
        ChangePasswordRequestEntity changePasswordRequestEntity = changePasswordRequestRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        User user = changePasswordRequestEntity.getUser();
        user.changePassword(changePasswordRequestEntity.getPassword());
        userRepository.save(user);
        return requestUtils.buildRequestAfterChangePassword();
    }

    public void createChangeEmailRequest(ChangeEmailRequest changeEmailRequest) {
        User user = findByEmail(changeEmailRequest.getOldEmail());
        if (user == null) {
            return;
        }
        var changeEmailRequestEntity = new ChangeEmailRequestEntity(user, changeEmailRequest.getOldEmail(),
                changeEmailRequest.getNewEmail());
        changeEmailRequestRepository.save(changeEmailRequestEntity);
        emailService.sendChangeEmail(changeEmailRequestEntity.getId(),
                changeEmailRequest.getOldEmail(),
                changeEmailRequest.getNewEmail());
    }

    private User findUserByLoginOrEmail(String email, String login) {
        User checkUser = userRepository.findByLogin(login);
        if (checkUser != null) {
            return checkUser;
        }
        checkUser = userRepository.findByEmail(email);
        if (checkUser != null) {
            return checkUser;
        }
        throw new ControlFailedException("User not found for change password",
                new ErrorInfo(ErrorConstants.ERROR_USER_INPUT, NOT_FOUND_USER_FOR_SEND_EMAIL));
    }

    public URI confirmChangeEmail(UUID id) {
        ChangeEmailRequestEntity changeEmailRequestEntity = changeEmailRequestRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        if (changeEmailRequestEntity.isValidDate()) {
            LinkedMultiValueMap<String, String> redirectParams = new LinkedMultiValueMap<>();
            throw new RedirectException("Not valid date to change email request " + id,
                    myCountryArchitectParams.getRedirectAfterChangeEmail(),
                    redirectParams);
        }
        User user = changeEmailRequestEntity.getUser();
        user.changeEmail(changeEmailRequestEntity.getNewEmail());
        userRepository.save(user);
        return requestUtils.buildRequestAfterChangeEmail();
    }
}