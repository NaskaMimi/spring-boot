package ru.rife.mcarch.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import ru.rife.mcarch.dto.AuthDataResponse;
import ru.rife.mcarch.exception.AuthException;
import ru.rife.mcarch.security.JwtUtil;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthService {
    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetailsService;
    private final JwtUtil jwtUtil;

    public AuthDataResponse authByLoginAndPassword(String login, String password) {

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(login, password)
            );
        } catch (BadCredentialsException e) {
            throw new AuthException("Incorrect username or password", e);
        }

        UserDetails userDetails = userDetailsService.loadUserByUsername(login);
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .map(this::removeRolePrefix)
                .toList();
        JwtUtil.JwtRich jwtRich = jwtUtil.generateToken(userDetails);
        return new AuthDataResponse(jwtRich.getToken(), jwtRich.getExpiredAt(), roles);
    }

    private String removeRolePrefix(String text) {
        return text.replace("ROLE_", "");
    }
}
