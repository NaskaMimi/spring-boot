package ru.rife.mcarch.service;

import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.rife.mcarch.util.RequestUtils;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class EmailService {
    private final RequestUtils requestUtils;
    private final JavaMailSender javaMailSender;

    public void sendApproveRegistration(String email,
                                        UUID createRequestId) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("noreply@country-modeling.com");
        message.setTo(email);
        message.setSubject("Регистрация");
        message.setText("Добро пожаловать в Country service modeling\n" +
                "Для регистрации передите по ссылке " +
                requestUtils.buildRequestConfirmRegistration(createRequestId)
        );
        javaMailSender.send(message);
    }

    public void sendNewPasswordToEmail(String email,
                                       String originalPassword) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("noreply@country-modeling.com");
        message.setTo(email);
        message.setSubject("Регистрация");
        message.setText("Спасибо за регистрацию!\n" +
                "Ваш пароль для входа \"" + originalPassword + "\""
        );
        javaMailSender.send(message);
    }

    public void sendChangePassword(String email,
                                   UUID changeEmailRequestId) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("noreply@country-modeling.com");
        message.setTo(email);
        message.setSubject("Регистрация");
        message.setText("Для изменения пароля перейдите пожалуйста по ссылке\n" +
                requestUtils.buildRequestConfirmChangePassword(changeEmailRequestId)
        );
        javaMailSender.send(message);
    }

    public void sendChangeEmail(UUID id, String oldEmail, String newEmail) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("noreply@country-modeling.com");
        message.setTo(oldEmail);
        message.setSubject("Регистрация");
        message.setText("Для изменения email на \"" + newEmail + "\" перейдите пожалуйста по ссылке\n" +
                requestUtils.buildRequestConfirmChangeEmail(id)
        );
        javaMailSender.send(message);
    }
}
