package ru.rife.mcarch.service;

import lombok.RequiredArgsConstructor;
import ru.rife.mcarch.entity.User;
import ru.rife.mcarch.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.rife.mcarch.security.GuestUserAuth;

@Service
@RequiredArgsConstructor
public class DbUserDetailService implements UserDetailsService {
    private final UserRepository userRepository;
    private final GuestUserAuth guestUserAuth;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(login);
        UserDetails guestUserDetail = guestUserAuth.generateTokenForGuest(login);
        if (guestUserDetail != null) {
            return guestUserDetail;
        }

        if (user == null) {
            throw new UsernameNotFoundException("Unknown user by login: " + login);
        }
        return org.springframework.security.core.userdetails.User.builder()
                .username(user.getLogin())
                .password(user.getPassword())
                .roles("USER")
                .build();
    }
}
